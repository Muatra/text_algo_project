var nombreDeCaracteres = 256;
class Rabin_Karp{
    constructor(texte,chaine,nombrePremier,obj){
        this.texte = texte;
        this.chaine = chaine;
        this.nombrePremier=nombrePremier;
        
        this.hashChaine=0;
        this.hashTexte=0;
        this.hashValeur=1;
        
        this.index;
        this.indice;
        
        this.obj = obj;
    }
    recherche(){
        for(this.index=0;this.index<this.chaine.length-1;this.index++){
            this.hashValeur = (this.hashValeur*nombreDeCaracteres)%this.nombrePremier;
        }
        //Calcul la valeur de hachage de la chaine et de la premiere portion du texte
        for(this.index = 0; this.index < this.chaine.length;this.index++){
            this.hashChaine = (nombreDeCaracteres*this.hashChaine + this.chaine.charCodeAt(this.index))%this.nombrePremier;
            this.hashTexte = (nombreDeCaracteres*this.hashTexte + this.texte.charCodeAt(this.index))%this.nombrePremier;
        }
        //Fais glisser la chaine sur le texte 
        for(this.index=0;this.index<=this.texte.length - this.chaine.length;this.index++){
            //Compare la valeur de hachage de la chaine et de la portion du texte actuelle
            //Si la valeur de hachage est égale alors on regarde ensuite les lettres une par une
            if(this.hashChaine===this.hashTexte){
                //On compare les caractères/lettres une par une
                for(this.indice = 0;this.indice<this.chaine.length;this.indice++){
                    if(this.texte.charCodeAt(this.indice+this.index)!==this.chaine.charCodeAt(this.indice))
                        break;
                }
                // if hashChaine == hashTexte et chaine.charCodeAt([0...this.chaine.length-1]) = this.texte.charCodeAt([index, index+1, ...index+this.chaine.length-1])
                if(this.indice===this.chaine.length){
                    this.obj.append($("<p class='result'>").text("Chaine trouvé à l'indice "+(this.index+1)));
                    //return ("Chaine trouvé à l'indice "+this.index);
                }
            }
            //Calcul la valeur de hachage de la prochaine portion du texte en utilisant la propriété de l'empreinte de Rabin : retirer le bit de poids fort du hash courant et ajouter le bit de poids faible du hash suivant
            if(this.index < this.texte.length - this.chaine.length){
                this.hashTexte = (nombreDeCaracteres*(this.hashTexte - this.texte.charCodeAt(this.index)*this.hashValeur)+this.texte.charCodeAt(this.index+this.chaine.length))%this.nombrePremier;
                //au cas où le hash du texte est négative
                if(this.hashTexte < 0)
                    this.hashTexte = (this.hashTexte + this.nombrePremier);
            }
            
        }
        
    }

}

function init(){
    $(".result").remove();
    var div = $("#start");
    var textarea = $("#text");
    var input = $("input");
    if(textarea.val()!="" && input.val()!=""){
        var rk = new Rabin_Karp(textarea.val(),input.val(),101,div);
        //div.append($("<p class='result'>").text("Texte : "+textarea.val()));
        //div.append($("<p class='result'>").text("Chaine : "+input.val()));
        rk.recherche();
        $(".result").hide();
        $(".result").show(800);
    }
    else{
        alert("Texte et/ou motif vide !");
    }
}
function remove(){
    $('.result').hide(400,function(){
        $(this).remove();
    });
}

//Voir pour afficher et colorier les endroits où la sous chaine apparait dans le texte